import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

import { actionsUser } from '../Actions/actionsUser';
import logo from '../logo.svg';
import './home.css';


class Home extends React.Component {
  componentDidMount() {
    this.props.getUsers();
}

render() {
  const { user } = this.props;
  const bull = <span className="bullet">•</span>;

  return (
    <Grid container justify="center" alignItems="center">
        <Grid item xs={4} sm={4}></Grid>
        <Grid item xs={4} sm={4}>
            <img src={logo} className="App-logo" alt="logo" />
        </Grid>
        <Grid item xs={4} sm={4}></Grid>
        <Grid item xs={6} sm={3}></Grid>
        <Grid item xs={6} sm={6}>
            <div className="root">
                <Paper elevation={6} className="paper"> 
                    <Typography variant="h2" component="h2" align="center" className="color">
                        {bull}BIENVENIDO{bull}
                    </Typography>
                    <Typography variant="h6" component="p" align="center">
                        Ha iniciado de manera exitosa
                    </Typography>
                    <Typography variant="h4" align="center" >
                        *{user.username}*
                    </Typography>
                    <Link to="/login">Logout</Link>
                </Paper>
            </div>
        </Grid>
        <Grid item xs={6} sm={3}></Grid>
    </Grid>
  );

}
}

function mapState(state) {
  const { authentication } = state;
  const { user } = authentication;
  return { user };
}

const actionCreators = {
  getUsers: actionsUser.getAll,
}

const connectedHome = connect(mapState, actionCreators)(Home);
export { connectedHome as Home };