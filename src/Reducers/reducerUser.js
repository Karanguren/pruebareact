import { constantsUser } from '../Constants/constantsUser';

export function users(state = {}, action) {
  switch (action.type) {
    case constantsUser.GETALL_REQUEST:
      return {
        loading: true
      };
    case constantsUser.GETALL_SUCCESS:
      return {
        items: action.users
      };
    case constantsUser.GETALL_FAILURE:
      return { 
        error: action.error
      };
    case constantsUser.DELETE_REQUEST:
      // add 'deleting:true' property to user being deleted
      return {
        ...state,
        items: state.items.map(user =>
          user.id === action.id
            ? { ...user, deleting: true }
            : user
        )
      };
    case constantsUser.DELETE_SUCCESS:
      // remove deleted user from state
      return {
        items: state.items.filter(user => user.id !== action.id)
      };
    case constantsUser.DELETE_FAILURE:
      // remove 'deleting:true' property and add 'deleteError:[error]' property to user 
      return {
        ...state,
        items: state.items.map(user => {
          if (user.id === action.id) {
            // make copy of user without 'deleting:true' property
            const { deleting, ...userCopy } = user;
            // return copy of user with 'deleteError:[error]' property
            return { ...userCopy, deleteError: action.error };
          }

          return user;
        })
      };
    default:
      return state
  }
}