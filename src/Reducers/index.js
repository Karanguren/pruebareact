import { combineReducers } from 'redux';

import { authentication } from './reducerAuthentication';
import { registration } from './reducersRegister';
import { users } from './reducerUser';
import { alert }  from './reducerAlert';

const rootReducer = combineReducers({
  authentication,
  registration,
  users,
  alert
});

export default rootReducer;