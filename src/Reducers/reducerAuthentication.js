import { constantsUser } from '../Constants/constantsUser';

let user = JSON.parse(localStorage.getItem('user'));
const initialState = user ? { loggedIn: true, user } : {};

export function authentication(state = initialState, action) {
  switch (action.type) {
    case constantsUser.LOGIN_REQUEST:
      return {
        loggingIn: true,
        user: action.user
      };
    case constantsUser.LOGIN_SUCCESS:
      return {
        loggedIn: true,
        user: action.user
      };
    case constantsUser.LOGIN_FAILURE:
      return {};
    case constantsUser.LOGOUT:
      return {};
    default:
      return state
  }
}