import React from 'react';
import { useDispatch, useSelector } from "react-redux";

import Snackbar from "@material-ui/core/Snackbar";
import IconButton from "@material-ui/core/IconButton";
import { clearSnackbar } from "../Actions/actionsAlert";
import Button from '@material-ui/core/Button';
import CloseIcon from '@material-ui/icons/Close';



export default function SnackbarL() {
  const dispatch = useDispatch();

  // const {SnackbarMessage, SnackbarOpen } = useSelector(
  //   state => state.ui
  // );

  function handleClose() {
    dispatch(clearSnackbar());
  }

  return (
    <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        // open={SnackbarOpen}
        autoHideDuration={6000}
        onClose={handleClose}
        // message={SnackbarMessage}
        action={
          <React.Fragment>
            <Button color="secondary" size="small" onClick={handleClose}>
              UNDO
            </Button>
            <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
              <CloseIcon fontSize="small" />
            </IconButton>
          </React.Fragment>
        }
      />
  );

}