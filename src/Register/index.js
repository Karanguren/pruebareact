import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';


import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';


import './register.css';
import { actionsUser } from '../Actions/actionsUser';
import { actionsAlert } from '../Actions/actionsAlert';

class Register extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            user: {
                email: '',
                username: '',
                password: '',
                confirmPassword: '',
            },
            submitted: false,
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    
    handleChange(event) {

        const { user } = this.state;

        const { name, value } = event.target;
        this.setState({
            user: {
                ...user,
                [name]: value
            }
        }); 

    }

    handleSubmit(event) {
        event.preventDefault();
        this.setState({ submitted: true });
        const { user } = this.state;

        var email = document.getElementById('email');
        var filter = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        
        if (!filter.test(email.value)) {
            this.props.errorE('Please provide a valid email address');
        }
        if (user.password !== user.confirmPassword) {
            this.props.errorE("Passwords don't match");
        }
         else if( user.email && user.username && user.password) {
             this.props.register(user);
        }
    }

    render() {
        const { registering  } = this.props;
        const { user, submitted} = this.state;

        return (
            <Grid container justify="center" alignItems="center">
                <Grid item xs={10} sm={4}></Grid>
                <Grid item xs={10} sm={4}>
                    <Paper elevation={6} className="paperR"> 
                        <Avatar className="avatarR">
                            <LockOutlinedIcon />
                        </Avatar>
                        <Typography component="h1" variant="h5">
                        Register
                        </Typography>
                        <form name="form" className="form" onSubmit={this.handleSubmit}>
                            <TextField
                                id="email"
                                value={user.email}
                                onChange={this.handleChange}
                                margin="normal"
                                label="Email"
                                name="email"
                                autoComplete="email"
                                fullWidth
                                autoFocus
                                error={(submitted && !user.email ? true : '')}
                                helperText={submitted && !user.email && "Email is required"}
                            />
                            <TextField
                                id="username"
                                value={user.username}
                                onChange={this.handleChange}
                                margin="normal"
                                label="Username"
                                name="username"
                                autoComplete="username"
                                fullWidth
                                error={(submitted && !user.username ? true : '')}
                                helperText={submitted && !user.username && "Username is required"}
                            />
                            <TextField
                                id="password"
                                value={user.password}
                                onChange={this.handleChange}
                                margin="normal"
                                name="password"
                                label="Password"
                                type="password"
                                autoComplete="current-password"
                                fullWidth
                                error={(submitted && !user.password ? true : '')}
                                helperText={submitted && !user.password && "Password is required"}
                            />
                            <TextField
                                id="confirmPassword"
                                value={user.confirmPassword}
                                onChange={this.handleChange}
                                margin="normal"
                                fullWidth
                                name="confirmPassword"
                                label="Confirm Password"
                                type="password"
                                error={(submitted && !user.confirmPassword ? true : '')}
                                helperText={submitted && !user.confirmPassword && "Password is required"}
                            />
                            <Button
                                type="submit"
                                variant="contained"
                                color="primary"
                                className="submit"
                                fullWidth
                            >
                                Register
                                {registering && <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />}
                            </Button>
                            <br />
                            <Link to="/login">Cancel</Link>
                        </form>    
                    </Paper>
                </Grid>
                <Grid item xs={10} sm={4}></Grid>
            </Grid>    
        );
    }
}

function mapState(state) {
    const { registering } = state.registration;
    return { registering };
}

const actionCreators = {
    register: actionsUser.register,
    errorE: actionsAlert.error,
    clear: actionsAlert.clear

}

const connectedRegister = connect(mapState, actionCreators)(Register);
export { connectedRegister as Register };




  