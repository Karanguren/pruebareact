import React from 'react';
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import Grid from '@material-ui/core/Grid';

import { history } from './Components/history';
import { actionsAlert } from './Actions/actionsAlert';
import { PrivateRoute } from './Components/privateRouter';
import { Home } from './Home';
import { Login } from './Login';
import { Register } from './Register';

import "./AppStyle.css"

class App extends React.Component {
    constructor(props) {
        super(props);

        history.listen((location, action) => {
            this.props.clearAlerts();
        });
    }

    render() {
        const { alert } = this.props;
        return (
            <Grid container>
                <Grid item xs={12} sm={12}>
                    {alert.message &&
                        <div className={`alert ${alert.type}`}>{alert.message}</div>
                    }
                    <Router history={history}>
                        <Switch>
                            <PrivateRoute exact path="/" component={Home} />
                            <Route path="/login" component={Login} />
                            <Route path="/register" component={Register} />
                            <Redirect from="*" to="/" />
                        </Switch>
                    </Router>
                </Grid>
            </Grid>
        );
    }
}

function mapState(state) {
    const { alert } = state;
    return { alert };
}

const actionCreators = {
    clearAlerts: actionsAlert.clear
};

const connectedApp = connect(mapState, actionCreators)(App);
export { connectedApp as App };
