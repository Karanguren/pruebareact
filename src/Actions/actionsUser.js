import { constantsUser } from '../Constants/constantsUser';
import { serviceUser } from '../Service/serviceUser';
import { actionsAlert } from './actionsAlert';
import { history } from '../Components/history';

export const actionsUser = {
    login,
    logout,
    register,
    getAll,
    // delete: _delete
};

function login(username, password) {
    return dispatch => {
        dispatch(request({ username }));

        serviceUser.login(username, password)
            .then(
                user => { 
                    dispatch(success(user));
                    history.push('/');
                },
                error => {
                    dispatch(failure(error));
                    dispatch(actionsAlert.error(error));
                }
            );
    };

    function request(user) { return { type: constantsUser.LOGIN_REQUEST, user } }
    function success(user) { return { type: constantsUser.LOGIN_SUCCESS, user } }
    function failure(error) { return { type: constantsUser.LOGIN_FAILURE, error } }
}

function logout() {
    serviceUser.logout();
    return { type: constantsUser.LOGOUT };
}

function register(user) {
    return dispatch => {
        dispatch(request(user));

        serviceUser.register(user)
            .then(
                user => { 
                    dispatch(success());
                    history.push('/login');
                    dispatch(actionsAlert.success('Registration successful'));
                },
                error => {
                    dispatch(failure(error));
                    dispatch(actionsAlert.error(error));
                }
            );
    };

    function request(user) { return { type: constantsUser.REGISTER_REQUEST, user } }
    function success(user) { return { type: constantsUser.REGISTER_SUCCESS, user } }
    function failure(error) { return { type: constantsUser.REGISTER_FAILURE, error } }
}

function getAll() {
    return dispatch => {
        dispatch(request());

        serviceUser.getAll()
            .then(
                users => dispatch(success(users)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request(users) { return { type: constantsUser.GETALL_REQUEST, users } }
    function success(users) { return { type: constantsUser.GETALL_SUCCESS, users } }
    function failure(error) { return { type: constantsUser.GETALL_FAILURE, error } }
}

//prefixed function name with underscore because delete is a reserved word in javascript
// function _delete(id) {
//     return dispatch => {
//         dispatch(request(id));

//         serviceUser.delete(id)
//             .then(
//                 user => dispatch(success(id)),
//                 error => dispatch(failure(id, error.toString()))
//             );
//     };

//     function request(id) { return { type: userConstants.DELETE_REQUEST, id } }
//     function success(id) { return { type: userConstants.DELETE_SUCCESS, id } }
//     function failure(id, error) { return { type: userConstants.DELETE_FAILURE, id, error } }
// }